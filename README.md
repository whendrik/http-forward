# HTTP Forward methods

Different methods to HTTP(S) forward traffic

## Method 1 - `nginx`

Say you want to forward `8080` traffic to `http://127.0.0.1:54321`,

Start a `nginx` webserver with `nginx.conf`;

```
http {
		...

        server {
                listen 8080;
                location / {
                        proxy_pass http://127.0.0.1:54321;
                }

                access_log /root/nginx/access.log;
                error_log /root/nginx/error.log;
        }
}
```

## Method 2 - `tinyproxy`

Say you want to forward `5555` traffic to `http://127.0.0.1:54321`,

Start a `tinyproxy` with `tinyproxy.conf`;

```
Port 5555

...

ViaProxyName "tinyproxy"

ConnectPort 5555
ConnectPort 54321
ReversePath "/" "http://127.0.0.1:54321/"

ReverseOnly Yes
ReverseMagic Yes
ReverseBaseURL "http://127.0.0.1:54321/"
```
